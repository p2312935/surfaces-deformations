#ifndef SURFACE_H
#define SURFACE_H

#include <QVector>

#include "mesh.h"
#include "mathematics.h"
#include "interpolator.h"

class Surface
{
public:
    Surface(unsigned int u, unsigned int v);
    virtual Mesh Polygonize() const = 0;
    void Twist(QString axis, double period);
    void Attenuate(Vector c , Vector t);
    void AttenuateWyvill(Vector c , Vector t);

protected:
    double u_, v_;
    std::vector<std::vector<Vector>> points_;
};

// bezier surface
class BezierSurface : public Surface
{
public:
    BezierSurface(const std::vector<std::vector<Vector>>& control_points, unsigned int u, unsigned int v);
    virtual Mesh Polygonize() const override;

private:
    std::vector<std::vector<Vector>> control_points_;
};

class RevolutionSurface : public Surface
{
public:
    RevolutionSurface(const std::vector<Vector>& control_points, unsigned int u, unsigned int v);
    virtual Mesh Polygonize() const override;

private:
    std::vector<Vector> control_points_;
};


#endif // SURFACE_H
