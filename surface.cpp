#include "surface.h"
#include <iostream>
#include <cmath>

// surface
Surface::Surface(unsigned int u, unsigned int v) : u_(1.0/(double)(u)), v_(1.0/(double)(v))
{}




void Warp( Vector& point , double period , QString axis ) {
    if (axis=='z'){
        double alpha =  (2* M_PI *point[2]) / period  ;
        float x = point[0]*cos(alpha) - point[1]*sin(alpha);
        float y = point[0]*sin(alpha) + point[1]*cos(alpha);
        float z = point[2];
        point = Vector(x, y, z);
    }else if (axis=='y'){
            double alpha = (2 * M_PI * point[1]) / period;
            float x = point[0] * cos(alpha) + point[2] * sin(alpha);
            float y = point[1] ;
            float z = - point[0] * sin(alpha) + point[2] * cos(alpha);
            point = Vector(x, y, z);
    }else if (axis=='x'){
            double alpha = (2 * M_PI * point[0]) / period;
            float x = point[0];
            float y = point[1] * cos(alpha) - point[2] * sin(alpha);
            float z = point[1] * sin(alpha) + point[2] * cos(alpha);
            point = Vector(x, y, z);
    }

}



void Surface::Twist(QString axis, double period)
{
    for (unsigned int j = 0; j < points_.size(); ++j)
    {
        for (unsigned int i = 0; i < points_.at(j).size(); ++i)
        {
            Warp( points_.at(j).at(i) , period , axis);
        }
    }
}


double smoothstep(double edge0, double edge1, double x) {
    x = std::clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
    return x * x * (3 - 2 * x);
}

void Surface::Attenuate(Vector c , Vector t){

    for (int j = 0 ; j< points_.size() ; ++j){
        for ( int i = 0 ; i< points_.at(j).size(); ++i){
            double d = sqrt(std::pow(points_.at(j).at(i)[0] - c[0] , 2) + std::pow(points_.at(j).at(i)[1] - c[1],2) + std::pow(points_.at(j).at(i)[2] - c[2] , 2)) ;
            d= std::min(d/0.4,1.0);
            double g_p = smoothstep(0,std::pow(1 - std::pow(d,2) , 2),d);
            points_.at(j).at(i) =  Vector(points_.at(j).at(i)[0] + t[0]*g_p,points_.at(j).at(i)[1]+t[1]*g_p,points_.at(j).at(i)[2]+t[2]*g_p);
        }
    }
}


void Surface::AttenuateWyvill(Vector c , Vector t){

    for (int j = 0 ; j< points_.size() ; ++j){
        for ( int i = 0 ; i< points_.at(j).size(); ++i){
            double d = sqrt(std::pow(points_.at(j).at(i)[0] - c[0] , 2) + std::pow(points_.at(j).at(i)[1] - c[1],2) + std::pow(points_.at(j).at(i)[2] - c[2] , 2)) ;
            d= std::min(d,1.0);
            double g_p = std::pow(1 - std::pow(d ,2) , 2);
            points_.at(j).at(i) =  Vector(points_.at(j).at(i)[0] + t[0]*g_p,points_.at(j).at(i)[1]+t[1]*g_p,points_.at(j).at(i)[2]+t[2]*g_p);
        }
    }
}



BezierSurface::BezierSurface(const std::vector<std::vector<Vector> > &control_points, unsigned int u, unsigned int v)
    : Surface(u, v), control_points_(control_points)
{
    double t = 0.0;
    double j = 0.0;

    while(t < 1.0)
    {
        j = 0.0;
        std::vector<Vector> intermediate_points;
        intermediate_points.reserve(control_points_.size());

        for(unsigned int i = 0; i < control_points_.size(); ++i)
            intermediate_points.push_back(Interpolator::DeCastledjau(control_points_.at(i), t));

        std::vector<Vector> row_points;
        while(j < 1.0)
        {
            row_points.push_back(Interpolator::DeCastledjau(intermediate_points, j));
            j += v_;
        }

        points_.push_back(row_points);
        t += u_;
    }
}

Mesh BezierSurface::Polygonize() const
{
    std::vector<Vector> vertices;
    std::vector<Vector> normals;
    std::vector<int> vertex_indices;
    std::vector<int> normal_indices;

    for(unsigned int i = 0; i < points_.size(); ++i)
    {
        for(unsigned int j = 0; j < points_.at(i).size(); ++j)
        {
            vertices.push_back(points_.at(i).at(j));
            normals.push_back(Vector(0.0, 1.0, 0.0));
            if(i < points_.size()-1 && j < points_.at(i).size()-1)
            {
                vertex_indices.push_back(i*points_.at(i).size()+j);
                vertex_indices.push_back((i+1)*points_.at(i).size()+j);
                vertex_indices.push_back((i+1)*points_.at(i).size()+j+1);

                vertex_indices.push_back(i*points_.at(i).size()+j);
                vertex_indices.push_back((i+1)*points_.at(i).size()+j+1);
                vertex_indices.push_back(i*points_.at(i).size()+j+1);

                normal_indices.push_back(i*points_.at(i).size()+j);
                normal_indices.push_back((i+1)*points_.at(i).size()+j);
                normal_indices.push_back((i+1)*points_.at(i).size()+j+1);

                normal_indices.push_back(i*points_.at(i).size()+j);
                normal_indices.push_back((i+1)*points_.at(i).size()+j+1);
                normal_indices.push_back(i*points_.at(i).size()+j+1);
            }
        }
    }

    Mesh mesh(vertices, normals, vertex_indices, normal_indices);
    mesh.SmoothNormals();
    return mesh;
}

// revolution surface
RevolutionSurface::RevolutionSurface(const std::vector<Vector> &control_points, unsigned int u, unsigned int v)
    : Surface(u-1, v), control_points_(control_points)
{
    double t = 0.0;
    double a = 2.0*M_PI *v_;
    double epsilon = 0.001;
    while (t <= 1.0+epsilon)
    {
        Vector p = Interpolator::DeCastledjau(control_points_, t);
        // X
        std::vector<Vector> circle_points;
        double theta = 0.0;
        while(theta <= (2.0*M_PI )-a)
        {
            circle_points.push_back(Vector(p[0], (cos(theta)*p[1])+(sin(theta)*p[2]), (-sin(theta)*p[1])+(cos(theta)*p[2])));
            theta += a;
        }
        points_.push_back(circle_points);
        t += u_;
    }
}


Mesh RevolutionSurface::Polygonize() const
{

    std::vector<Vector> vertices;
    std::vector<Vector> normals;
    std::vector<int> vertex_indices;
    std::vector<int> normal_indices;

    for(unsigned int i = 0; i < points_.size(); ++i)
    {
        for(unsigned int j = 0; j < points_.at(i).size(); ++j)
        {
            vertices.push_back(points_.at(i).at(j));
            normals.push_back(Vector(0.0, 1.0, 0.0));
            unsigned int next = (j+1) % points_.at(i).size();
            if(i < points_.size()-1)
            {
                vertex_indices.push_back(i*points_.at(i).size()+j);
                vertex_indices.push_back(i*points_.at(i).size()+next);
                vertex_indices.push_back((i+1)*points_.at(i).size()+next);

                vertex_indices.push_back(i*points_.at(i).size()+j);
                vertex_indices.push_back((i+1)*points_.at(i).size()+next);
                vertex_indices.push_back((i+1)*points_.at(i).size()+j);

                normal_indices.push_back(i*points_.at(i).size()+j);
                normal_indices.push_back(i*points_.at(i).size()+next);
                normal_indices.push_back((i+1)*points_.at(i).size()+next);

                normal_indices.push_back(i*points_.at(i).size()+j);
                normal_indices.push_back((i+1)*points_.at(i).size()+next);
                normal_indices.push_back((i+1)*points_.at(i).size()+j);
            }
        }
    }

    Mesh mesh(vertices, normals, vertex_indices, normal_indices);
    mesh.SmoothNormals();
    return mesh;
}
