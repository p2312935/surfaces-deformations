#include "qte.h"
#include "implicits.h"
#include "ui_interface.h"
#include "interpolator.h"
#include "surface.h"
#include <iostream>
#include <chrono>
#include <random>
#include <cstdlib>
MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	// Buttons
    connect(uiw->Bezier, SIGNAL(clicked()), this, SLOT(Bezier()));
    connect(uiw->Revolution, SIGNAL(clicked()), this, SLOT(Revolution()));
    connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}









void MainWindow::Bezier()
{
    auto start_time = std::chrono::high_resolution_clock::now();

    std::vector<std::vector<Vector>> control_points;
    control_points.resize(10); // 10 rows
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 30; j++) {
            double x = static_cast<double>(i) / 10.0;
            double y = static_cast<double>(j) / 30.0;
             //double z = (static_cast<double>(rand()) / RAND_MAX) * 2.0 - 1.0;
             double z = 0.05 * sin(2 * M_PI * x) + 0.05 * cos(2 * M_PI * y);
             // double z=0;
            control_points[i].push_back(Vector(x, y, z));
        }
    }

    BezierSurface surface(control_points, uiw->ures->text().toInt(), uiw->vres->text().toInt());
    if(uiw->lambda->text().toFloat())
        surface.Twist(uiw->comboBox->currentText(),uiw->lambda->text().toDouble());
    // surface.AttenuateWyvill(control_points.at(5).at(15) , Vector(0,0,1));
     surface.Attenuate(control_points.at(5).at(15) , Vector(0,0,0.5) );
     surface.Attenuate(control_points.at(5).at(15) , Vector(0.3,0,0) );
     surface.Attenuate(control_points.at(5).at(15) , Vector(0,0.3,0) );
    Mesh m = surface.Polygonize();
    std::vector<Color> cols;
    cols.resize(m.Vertexes());
    for (int i = 0; i < int(cols.size()); i++)
        cols[i] = Color(0.8, 0.8, 0.8);
    meshColor = MeshColor(m, cols, m.VertexIndexes());
    UpdateGeometry();
    auto end_time = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);
    uiw->time->setText(QString::number(duration.count()));
    m.SaveObj("bez.obj" , "BoiteMesh");
}


void MainWindow::Revolution(){
    auto start_time = std::chrono::high_resolution_clock::now();

        std::vector<Vector> control_points;
        control_points.push_back(Vector(0.0, 0.0, 0.1));
        control_points.push_back(Vector(0.2, 0.0, 0.6));
        control_points.push_back(Vector(0.4, 0.0, 1.0));
        control_points.push_back(Vector(0.6, 0.0, 0.6));
        control_points.push_back(Vector(1.0, 0.0, 0.1));
        control_points.push_back(Vector(1.0, 0.0, 0.1));
        control_points.push_back(Vector(2.2, 0.0, 0.6));
        control_points.push_back(Vector(1.4, 0.0, 1.0));
        control_points.push_back(Vector(1.6, 0.0, 0.6));
        control_points.push_back(Vector(2.0, 0.0, 0.1));
        control_points.push_back(Vector(3.0, 0.0, 0.1));
        control_points.push_back(Vector(3.2, 0.0, 0.6));
        control_points.push_back(Vector(3.4, 0.0, 1.0));
        control_points.push_back(Vector(3.6, 0.0, 0.6));
        control_points.push_back(Vector(3.0, 0.0, 0.1));

        RevolutionSurface surface(control_points, uiw->ures->text().toInt(), uiw->vres->text().toInt());
        if(uiw->lambda->text().toFloat())
            surface.Twist(uiw->comboBox->currentText(),uiw->lambda->text().toDouble());
        Mesh m = surface.Polygonize();
        std::vector<Color> cols;
        cols.resize(m.Vertexes());
        for (int i = 0; i < int(cols.size()); i++)
        cols[i] = Color(0.8, 0.8, 0.8);
        meshColor = MeshColor(m, cols, m.VertexIndexes());
        UpdateGeometry();
        auto end_time = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        uiw->time->setText(QString::number(duration.count()));
        m.SaveObj("rev.obj" , "BoiteMesh");
}











void MainWindow::BoxMeshExample()
{
	Mesh boxMesh = Mesh(Box(1.0));

	std::vector<Color> cols;
	cols.resize(boxMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::SphereImplicitExample()
{
  AnalyticScalarField implicit;

  Mesh implicitMesh;
  implicit.Polygonize(31, implicitMesh, Box(2.0));

  std::vector<Color> cols;
  cols.resize(implicitMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
    cols[i] = Color(0.8, 0.8, 0.8);

  meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
